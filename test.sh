#!/bin/sh

#-------------------------------------------------------------------------------------------------
# Ce petit script permet seulement de lancer firefox pour effectuer des requetes HTTP et HTTPS
# pour tester le fonctionnement du proxy
#-------------------------------------------------------------------------------------------------

firefox https://www.google.fr; # requete HTTPS
firefox http://www.onisep.fr/ # requete HTTP
