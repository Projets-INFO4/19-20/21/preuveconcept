>  # PreuveConcept

Section Preuve Concept du projet "Proxy Cache HTTPS"

-------------------------------------------------------------------------------
# 1er Preuve réalisée :

## Installations nécessaires
* Mitmproxy : https://docs.mitmproxy.org/stable/ -> Jouer un jeu de requete GET

* Cproxy : https://github.com/coiby/cproxy -> Mettre en place un cache HTTP/HTTPS

* Wireshark

## Etapes :
1. Lancer Mitmdumb avec l'option -w pour sauvegarder le flux dans un fichier 
* Exemple de commande : `mitmdumb -w outfile`
* Rejouer le flux : `mitmdumb -nC outfile`

2. Lancer Cproxy, puis naviguer sur Internet afin de remplir le cache HTTPS -> `$ cproxy`

3. Couper Cproxy et le lancer de nouveau avec cette fois avec l'option -off pour activer l'option offline -> `$ cproxy -off`

4. Rejouer les requetes du début

5. A l'aide de Wireshark, nous devrions voir l'échange entre le client et le proxy qui lui répond grace aux informations dans son buffer (Cas souhaité)

**|Problème|**
Au cours de cette preuve, nous avons relevé une incompatibilité entre Mitmproxy et Cproxy car les deux ne peuvent être lancés en même temps et travaillé sur
le même port.

**|Solution|**
Pour cela, nous avons décidé de mettre de coté Mitmproxy, et de créer un petit script afin d'effectuer des requêtes HTTP et HTTPS.

-------------------------------------------------------------------------------
# 2eme Preuve réalisée :

## Installations nécessaires

* Cproxy : https://github.com/coiby/cproxy -> Mettre en place un cache HTTP/HTTPS

* Wireshark

* Script Bash : "test.sh"


## Code test.sh

    #!/bin/sh 
    firefox https://www.google.fr; # requete HTTPS
    firefox http://www.onisep.fr/ # requete HTTP

## Etapes :

1. Lancer Cproxy -> `$ cproxy` 

2. Lancer le script -> `$ ./test.sh`

3. Couper Cproxy et le lancer de nouveau avec cette fois avec l'option -off pour activer l'option offline -> `$ cproxy -off`

4. Rejouer les requetes du début

5. A l'aide de Wireshark, nous voyons l'échange entre le client et le proxy qui lui répond grace aux informations dans son buffer

**|Résultat|**
Nous obtenons bien le résultat souhaité, c'est à dire un buffer HTTP/HTTPS qui répond aux requètes du client.
Maintenant nous allons analyser en profondeur le code de Cproxy afin de mieux comprendre son fonctionnement et récupérer les lignes de code clés 
pour ensuite les rajouter eventuellement dans l'outil de géneration d'image système Kameleon


-------------------------------------------------------------------------------
# 3ème Preuve réalisée - Solution intermédaire: Squid :

## Installations nécessaires

* Squid -> sudo apt-get install `$squid`

* Webmin

## Etapes :

### HTTP

1. Connexion Webmin (browser: 127.0.0.1:10000)
 
2. Lancement Squid

3. Navigation web (onisep.fr)

4. Arret de la liasion internet du serveur

5. Le client réinterroge le proxy (analyse wireshark)

**|Résultat|**
Le proxy rejoue bien la requête présente dans le cache. 
Test en HTTPS: NOK --> Il faut réaliser des configurations particulières

### HTTPS

1. Configurer l'environnement HTTPS / SSL de Squid --> `$ openssl.cnf`

2. Créez une autorité de certification auto-signée --> `$ openssl req -new -newkey rsa:2048 -sha256 -days 365 -nodes -x509 -extensions v3_ca -keyout myCA.pem -out myCA.pem`

3. Import du ceritificat sur le browser client

4. Navigation HTTPS 

-------------------------------------------------------------------------------
# 5ème Preuve réalisée (Auto-certification HTTPS) :

## Installations nécessaires

* Mitmproxy ou Cproxy

## Etapes :

1. Lancer l'un des outils ci-dessus

2. Ensuite configurer le proxy du browser pour écouter le port 8080 en localhost

3. Visiter le site ['http://cp.ca'](http://cp.ca) pour accepter et installer la certification CA qui provient de Mitmproxy

4. L'utilisation des outils Mitmproxy et Cproxy est opérationnel maintenant

**|Indication|**
Cproxy étant un projet en cours de développement, est basé sur l'autorité certification de Mitmproxy.
