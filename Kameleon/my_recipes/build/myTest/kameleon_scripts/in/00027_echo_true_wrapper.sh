#!/usr/bin/env bash

ROOT_DIRECTORY=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

function post_exec_wrapper {
    echo $? > "$ROOT_DIRECTORY/bash_status"
    # Print end flags
    echo -n __CMD_ERR_27_14594_12691_END__ 1>&2
    echo -n __CMD_OUT_27_14594_12691_END__
}

function pre_exec_wrapper {
    # Print begin flags
    echo -n __CMD_ERR_27_14594_12691_BEGIN__ 1>&2
    echo -n __CMD_OUT_27_14594_12691_BEGIN__
}

trap 'post_exec_wrapper' INT TERM EXIT

## Started
pre_exec_wrapper
bash --rcfile "kameleon_scripts/in/bash_rc" kameleon_scripts/in/00027_echo_true.sh

