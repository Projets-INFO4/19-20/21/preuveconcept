#!/usr/bin/env bash

ROOT_DIRECTORY=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

function post_exec_wrapper {
    echo $? > "$ROOT_DIRECTORY/bash_status"
    # Print end flags
    echo -n __CMD_ERR_29_14594_5516_END__ 1>&2
    echo -n __CMD_OUT_29_14594_5516_END__
}

function pre_exec_wrapper {
    # Print begin flags
    echo -n __CMD_ERR_29_14594_5516_BEGIN__ 1>&2
    echo -n __CMD_OUT_29_14594_5516_BEGIN__
}

trap 'post_exec_wrapper' INT TERM EXIT

## Started
pre_exec_wrapper
bash --rcfile "/home/gaetan/Documents/Projet_Info4/my_recipes/build/myTest/kameleon_scripts/local/bash_rc" /home/gaetan/Documents/Projet_Info4/my_recipes/build/myTest/kameleon_scripts/local/00029_echo_true.sh

