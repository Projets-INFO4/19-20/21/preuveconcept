#!/usr/bin/env bash

ROOT_DIRECTORY=$(dirname $(readlink -f ${BASH_SOURCE[0]}))

function post_exec_wrapper {
    echo $? > "$ROOT_DIRECTORY/bash_status"
    # Print end flags
    echo -n __CMD_ERR_13_14594_158169_END__ 1>&2
    echo -n __CMD_OUT_13_14594_158169_END__
}

function pre_exec_wrapper {
    # Print begin flags
    echo -n __CMD_ERR_13_14594_158169_BEGIN__ 1>&2
    echo -n __CMD_OUT_13_14594_158169_BEGIN__
}

trap 'post_exec_wrapper' INT TERM EXIT

## Started
pre_exec_wrapper
bash --rcfile "/home/gaetan/Documents/Projet_Info4/my_recipes/build/myTest/kameleon_scripts/local/bash_rc" /home/gaetan/Documents/Projet_Info4/my_recipes/build/myTest/kameleon_scripts/local/00013_echo_testing_if_true_true.sh

